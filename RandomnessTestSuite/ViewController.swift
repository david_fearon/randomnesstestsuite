//
//  ViewController.swift
//  RandomnessTestSuite
//
//  Created by David Fearon on 23/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import Cocoa

class ViewController: NSViewController
{
    
    @IBOutlet weak var testArc4RandomButton: NSButton!
    
    @IBOutlet weak var label1: NSTextField!
    @IBOutlet weak var label2: NSTextField!
    
    @IBAction func didClickArc4RandomButton(_ sender: AnyObject)
    {
        testArc4RandomButton.title = "Testing..."
        
        DispatchQueue.global(attributes: DispatchQueue.GlobalAttributes.qosDefault).async { () -> Void in
            
            var blocks : [UInt32] = []
            
            for _ in 1...10000000
            {
                blocks.append(arc4random())
            }
            
            let bitSequence = BitSequence(blocks: blocks)
            
            let pValue = MonobitTest.pValueForBitSequence(bitSequence)
            
            let passed = pValue > 0.01
            
            let passString = passed ? "PASSES" : "FAILS"
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.label1.stringValue = "P-value for arc4random: \(pValue)"

                self.label2.stringValue = "arc4random() thus " + passString + " the monobit test for randomness"
                
                self.testArc4RandomButton.title = "Test arc4random()"
            });
        }
    }
}

