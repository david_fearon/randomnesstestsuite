//
//  BitSequence.swift
//  RandomnessTestSuite
//
//  Created by David Fearon on 23/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import Foundation

struct BitSequence : Sequence
{
    let blocks : [UInt32]
    
    var bitString : String
    {
        get
        {
            return bitStringFromBlocks()
        }
    }
    
    func makeIterator() -> BitSequenceGenerator
    {
        return BitSequenceGenerator(blocks: blocks)
    }
    
    func count() -> Int
    {
        return self.blocks.count * 32
    }
    
    private func bitStringFromBlocks() -> String
    {
        var bitString = ""
        
        for block in blocks
        {
            for index : UInt in (0...31).reversed()
            {
                let bit = BitwiseUtilities.bitValueAtIndex(UInt(block), index: index)
                bitString += "\(bit)"
            }
        }
        
        return bitString
    }
}

struct BitSequenceGenerator : IteratorProtocol
{
    private let blocks : [UInt32]
    
    private var index = 0
    
    init (blocks : [UInt32])
    {
        self.blocks = blocks
    }
    
    mutating func next() -> UInt32?
    {        
        if index < self.blocks.count
        {
            let block = self.blocks[index]
            index += 1
            return block
        }
        
        return nil
    }
}
