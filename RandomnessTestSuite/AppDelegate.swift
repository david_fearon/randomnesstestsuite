//
//  AppDelegate.swift
//  RandomnessTestSuite
//
//  Created by David Fearon on 23/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

}

