//
//  RandomnessTestSuiteTests.swift
//  RandomnessTestSuiteTests
//
//  Created by David Fearon on 23/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import XCTest

class RandomnessTestSuiteTests: XCTestCase
{
    func testBitValueAtIndex()
    {
        let testValue : UInt = 12 // 0b0001100
        
        var bit : UInt
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 0)
        XCTAssert(bit == 0)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 1)
        XCTAssert(bit == 0)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 2)
        XCTAssert(bit == 1)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 3)
        XCTAssert(bit == 1)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 4)
        XCTAssert(bit == 0)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 5)
        XCTAssert(bit == 0)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 6)
        XCTAssert(bit == 0)
        
        bit = BitwiseUtilities.bitValueAtIndex(testValue, index: 7)
        XCTAssert(bit == 0)
    }
    
    func testConvertedSumForBitSequence()
    {
        var bitSequence : BitSequence
        
        bitSequence = BitSequence(blocks : [12])
        
        XCTAssert(MonobitTest.convertedSumForBitSequence(bitSequence) == -28)
        
        bitSequence = BitSequence(blocks : [0])
        
        XCTAssert(MonobitTest.convertedSumForBitSequence(bitSequence) == -32)
        
        bitSequence = BitSequence(blocks : [0b11111111111111111111111111111111])
        
        XCTAssert(MonobitTest.convertedSumForBitSequence(bitSequence) == 32)
    }
    
    func testBitSequenceCountFunction()
    {
        let bitSequence = BitSequence(blocks : [12, 24, 0, 1, 9])
        
        XCTAssert(bitSequence.count() == 5 * 32)
    }
    
    func testBitStringFromBlocks()
    {
        var bitSequence = BitSequence(blocks : [12])
        
        XCTAssert(bitSequence.bitString == "00000000000000000000000000001100")
        
        bitSequence = BitSequence(blocks : [3652369854, 965774125])
        
        XCTAssert(bitSequence.bitString == "1101100110110010101111011011111000111001100100001000101100101101")

    }
}
