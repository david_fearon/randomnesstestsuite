//
//  BitwiseUtilities.swift
//  RandomnessTestSuite
//
//  Created by David Fearon on 24/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import Foundation

class BitwiseUtilities
{
    class func bitValueAtIndex(_ value : UInt, index: UInt) -> UInt
    {
        let returnBit = (value >> index) & UInt(1)
        
        return returnBit
    }
    
}
