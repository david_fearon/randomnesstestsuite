# README #

The Randomness Test Suite is a Swift implementation of a standard set of statistical randomness-testing techniques.

The techniques used are described in the [NIST paper](http://csrc.nist.gov/publications/nistpubs/800-22-rev1a/SP800-22rev1a.pdf) **_A Statistical Test Suite for 
Random and Pseudorandom Number Generators for Cryptographic Applications_**.

Currently only the [Monobit](https://bitbucket.org/david_fearon/randomnesstestsuite/src/9b23ec72cb12e21730e424243fbbc06d2d0bdbdc/RandomnessTestSuite/MonobitTest.swift?at=master&fileviewer=file-view-default) test is implemented.