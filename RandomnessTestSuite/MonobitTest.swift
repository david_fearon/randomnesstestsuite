//
//  MonobitTest.swift
//  RandomnessTestSuite
//
//  Created by David Fearon on 23/04/2016.
//  Copyright © 2016 David Fearon. All rights reserved.
//

import Cocoa

class MonobitTest
{
    /*
        Returns P-value for a given bit sequence by returning the complementary error function (erfc) of the monobit test statistic.
        A P-value of zero indicates the sequence appears completely non-random. 
        A P-value of 1 indicates the sequence appears perfectly random.
    */
    class func pValueForBitSequence(_ bitSequence : BitSequence) -> Float64
    {
        return erfc(testStatisticForBitSequence(bitSequence))        
    }
    
    /*
        Computes the test statistic for the monobit randomness test: the absolute value of the convertedSum Xi for the given bit sequence divided by the square root of the length of the sequence
    */
    class func testStatisticForBitSequence(_ bitSequence : BitSequence) -> Float64
    {        
        let absoluteSum = Float64(abs(convertedSumForBitSequence(bitSequence)))
        
        let n = Float64(bitSequence.count())
        
        return absoluteSum / sqrt(n)
    }
    
    /*
        Computes converted sum Sn = X1 + X2 + ... + Xn, where Xi = 2εi - 1 and ε is the bit sequence
    */
    class func convertedSumForBitSequence(_ bitSequence: BitSequence) -> Int
    {
        var convertedSum : Int = 0
        
        for block in bitSequence
        {
            for shift : UInt in 0...31
            {
                let bitValue = BitwiseUtilities.bitValueAtIndex(UInt(block), index: shift)
                
                let convertedValue = (Int(bitValue) * 2 - 1);
                
                convertedSum += convertedValue
            }
        }
        
        return convertedSum
    }
}
